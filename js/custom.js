$(document).ready(function(){
    $('.slick-slider').slick({
        infitite: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 5000,
        fade: true,
        cssEase: 'ease-in-out',
        pauseOnFocus: false,
        pauseOnHover: false,
        prevArrow: false,
        nextArrow: false,
    });


    //make hamburger to become a cross onclick event
    $(document).on('click', '.hamburger', function(){
        $('.hamburger span.first').toggleClass('rotate-first');
        $('.hamburger span.second').toggleClass('viz');
        $('.hamburger span.third').toggleClass('rotate-third');
        if($('.hamburger span.second').hasClass('viz')){
            $('.header nav ul.main-menu').css({
                    'opacity': '1',
                    'height': 'auto',
                    'transition': 'all ease-in-out .4s',
                    '-o-transition': 'all ease-in-out .4s',
                    '-webkit-transition': 'all ease-in-out .4s',
                    'overflow': 'visible'
                });
        }else{
            $('.header nav ul.main-menu').css({
                    'opacity': '0',
                    'height': '0',
                    'transition': 'all ease-in-out .4s',
                    '-o-transition': 'all ease-in-out .4s',
                    '-webkit-transition': 'all ease-in-out .4s'
                });
        }
    });

        // $(document).on('click', function(e) {
        //     if (!$(e.target).closest(".header nav").length) {
        //         $('.header nav ul.main-menu').css({
        //             'opacity': '0',
        //             'height': '0',
        //             'transition': 'all ease-in-out .4s'
        //         });
        //         $('.hamburger span.first').removeClass('rotate-first');
        //         $('.hamburger span.second').removeClass('viz');
        //         $('.hamburger span.third').removeClass('rotate-third');
        //     }
        //     e.stopPropagation();
        // });

    $('.header .main-menu .sub-menu').hover(function(){
        $('.drop-down').css({
            'height': 'auto',
            'opacity': '1',
            'transition': 'all ease-in-out .4s',
            '-o-transition': 'all ease-in-out .4s',
            '-webkit-transition': 'all ease-in-out .4s'
        });
    });
    $('.header .main-menu .sub-menu').mouseleave(function(){
        $('.drop-down').css({
            'height': '0',
            'opacity': '0',
            'transition': 'all ease-in-out .4s',
            '-o-transition': 'all ease-in-out .4s',
            '-webkit-transition': 'all ease-in-out .4s'
        });
    });

    $( window ).resize(function() {
        var windowWidth = $(this).outerWidth();

        if (windowWidth >= 731){
            if ($('.header nav ul.main-menu').css('opacity') == '0'){
                $('.header nav ul.main-menu').css({
                    'opacity': '1',
                    'height': 'auto',
                    'transition': 'all ease-in-out .4s',
                    '-o-transition': 'all ease-in-out .4s',
                    '-webkit-transition': 'all ease-in-out .4s'
                });
            }
        } else if (windowWidth < 730){
            if ($('.header nav ul.main-menu').css('opacity') == '1'){
                $('.header nav ul.main-menu').css({
                    'opacity': '0',
                    'height': '0',
                    'overflow': 'hidden',
                    'transition': 'all ease-in-out .4s',
                    '-o-transition': 'all ease-in-out .4s',
                    '-webkit-transition': 'all ease-in-out .4s'
                });
                $('.hamburger span.first').removeClass('rotate-first');
                $('.hamburger span.second').removeClass('viz');
                $('.hamburger span.third').removeClass('rotate-third');
            }
        }
    });

    //to top button
    $(function() {
        $(window).scroll(function() {
            if($(this).scrollTop() >= 50) {
                $('#toTop').fadeIn();
            } else {
            $('#toTop').fadeOut();
            }
        });
            $('#toTop').click(function() {
            $('body,html').animate({scrollTop:0},800);
        });
    });

    //slider text animation effect
    var fn=function(){
        $('.slick-slider .animated').removeClass("animated")
        setTimeout(arguments.callee,6000);
    }
        setTimeout( fn,6000 );

    //animation of search feild
    $( ".search input" ).focus(function() {

        $('.header .logo').css('opacity','0');
        $('.header .logo').css({
            'transition':'opacity ease-in-out .4s',
            '-o-transition': 'all ease-in-out .4s',
            '-webkit-transition': 'all ease-in-out .4s'
        });
        $('.header nav').css('opacity','0');
        $('.header nav').css({
            'transition':'opacity ease-in-out .4s',
            '-o-transition': 'all ease-in-out .4s',
            '-webkit-transition': 'all ease-in-out .4s'
        });

        $('.header .search').css({
            'animation': 'div-on-focus 2s',
            '-webkit-animation': 'div-on-focus 2s',
            'animation-delay': '.4s',
            '-webkit-animation-delay': '.4s',
            '-webkit-animation-fill-mode': 'forwards'
        });

        if($(window).width() < 400){
            $('.header .search input').css({
                'animation': 'input-on-focus-small 2s',
                '-webkit-animation': 'input-on-focus-small 2s',
                'animation-delay': '.4s',
                '-webkit-animation-delay': '.4s',
                '-webkit-animation-fill-mode': 'forwards'
            });
            $('.header .search input').attr('id', 'visible-placeholder');
        } else {
            $('.header .search input').css({
                'animation': 'input-on-focus 2s',
                '-webkit-animation': 'input-on-focus 2s',
                'animation-delay': '.4s',
                '-webkit-animation-delay': '.4s',
                '-webkit-animation-fill-mode': 'forwards'
            });
            $('.header .search input').attr('id', 'visible-placeholder');
        }

        $('.search img').css('opacity', '1');
        $('.search img').css({
            'transition': 'opacity ease-in-out .4s',
            '-webkit-transition': 'opacity ease-in-out .4s',
            '-o-transition': 'opacity ease-in-out .4s',
            'transition-delay': '.4s',
            '-webkit-transition-delay': '.4s',
            '-o-transition-delay': '.4s'
        });
    });

    $( ".search input" ).focusout(function() {
        $('.header .logo').css({
            'opacity': '1',
            'transition': 'opacity ease-in-out .4s',
            '-webkit-transition': 'opacity ease-in-out .4s',
            '-o-transition': 'opacity ease-in-out .4s',
            'transition-delay': '.4s',
            '-webkit-transition-delay': '.4s',
            '-o-transition-delay': '.4s'
        });
        $('.header nav').css({
            'opacity': '1',
            'transition': 'opacity ease-in-out .4s',
            '-webkit-transition': 'opacity ease-in-out .4s',
            '-o-transition': 'opacity ease-in-out .4s',
            'transition-delay': '2s',
            '-webkit-transition-delay': '2s',
            '-o-transition-delay': '2s'
        });

        $('.header .search').css({
            'animation': 'div-off-focus 2s',
            '-webkit-animation': 'div-off-focus 2s',
            '-webkit-animation-fill-mode': 'forwards'
        });

        if($(window).width() < 400){
            $('.header .search input').css({
                'animation': 'input-off-focus-small 2s',
                '-webkit-animation': 'input-off-focus-small 2s',
                '-webkit-animation-fill-mode': 'forwards'
            });
            $('.header .search input').attr('id','');
            $('.header .search input').val('');
        } else {
            $('.header .search input').css({
                'animation': 'input-off-focus 2s',
                '-webkit-animation': 'input-off-focus 2s',
                '-webkit-animation-fill-mode': 'forwards'
            });
            $('.header .search input').attr('id','');
            $('.header .search input').val('');
        }

        $('.search img').css({
            'opacity': '0',
            'transition': 'opacity ease-in-out .4s',
            '-webkit-transition': 'opacity ease-in-out .4s',
            '-o-transition': 'opacity ease-in-out .4s'
        });
    });

    //show modal onclick 'contact us'
    var contact = $('#contact-modal');

    $('#modal').click(function(){
        $('.modal-bg').css({
            'animation': 'modal-bg-show 1s',
            '-webkit-animation': 'modal-bg-show 1s',
            '-webkit-animation-fill-mode': 'forwards'
        });

        $('#contact-modal').css({
            'animation': 'modal-show 1s',
            '-webkit-animation': 'modal-show 1s',
            'animation-delay': '.5s',
            '-webkit-animation-delay': '.5s',
            '-webkit-animation-fill-mode': 'forwards'
        });

        $('body').css('overflow', 'hidden');


        //hide modal if click out of modal area
        jQuery(function($){
            $(document).mouseup(function (e){
                var div = $('#contact-modal .inner-modal');
                if (!div.is(e.target)
                    && div.has(e.target).length === 0) {

                    $('.modal-bg').css({
                        'animation': 'modal-bg-hide 2s',
                        '-webkit-animation': 'modal-bg-hide 2s',
                        '-webkit-animation-fill-mode': 'forwards'
                    });
                    $('#contact-modal').css({
                        'animation': 'modal-hide 1s',
                        '-webkit-animation': 'modal-hide 1s',
                        '-webkit-animation-fill-mode': 'forwards'
                    });

                    $('body').css('overflow', 'auto');
                }
            });
        });
    });


    //hide modal if click on close button
    $('#contact-modal .inner-modal .close').click(function(){
        $('.modal-bg').css({
            'animation': 'modal-bg-hide 2s',
            '-webkit-animation': 'modal-bg-hide 2s',
            '-webkit-animation-fill-mode': 'forwards'
        });
        $('#contact-modal').css({
            'animation': 'modal-hide 1s',
            '-webkit-animation': 'modal-hide 1s',
            '-webkit-animation-fill-mode': 'forwards'
        });

        $('body').css('overflow', 'auto');
    });


    $(function(){
        // phone mask
        $(".phone input").mask("+3 (999) 999-99-99");
    });




    //form fields validation
    $('form').submit(function(event){
        event.preventDefault();

        // var validName = true,
        //     validPhone = true,
        //     validEmail = true,
        //     validMessage= true;

        var validName = false,
            validPhone = false,
            validEmail = false,
            validMessage= false;

        var name = $('form .name input').val(),
            phone = $('form .phone input').val(),
            email = $('form .email input').val(),
            message = $('form .message textarea').val();

        var emptyName = 'Введите имя',
            unvalidName = 'Поле должно содержать только буквы',
            emptyPhone = 'Введите номер телефона',
            emptyEmail = 'Введите email',
            unvalidEmail = 'Некорректно заполненное поле',
            emptyMessage = 'Введите сообщение';

        $( 'form input' ).keypress(function(){
            $('form .valid p').removeClass('unvalid');
            $('form .valid p').css('display','none');
        });
        $( 'form textarea' ).keypress(function(){
            $('form .valid p').removeClass('unvalid');
            $('form .valid p').css('display','none');
        });

            if( validName == false || validPhone == false || validEmail == false || validMessage == false){
                if (!name){
                    $('form .name p span').html(emptyName);
                    $('form .name p').addClass('unvalid');
                    $('form .name p').css('display','block');
                } else if (!name.match(/^[A-Za-zА-Яа-яЁё\s]+$/)){
                    $('form .name p span').html(unvalidName);
                    $('form .name p').addClass('unvalid');
                    $('form .name p').css('display','block');
                } else {
                    $('form .name input').addClass('valid-input');
                    validName = true;
                }

                if (!phone){
                    $('form .phone p span').html(emptyPhone);
                    $('form .phone p').addClass('unvalid');
                    $('form .phone p').css('display','block');
                } else {
                    $('form .phone input').addClass('valid-input');
                    validPhone = true;
                }

                if (!email){
                    $('form .email p span').html(emptyEmail);
                    $('form .email p').addClass('unvalid');
                    $('form .email p').css('display','block');
                } else if (!email.match(/^[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[a-z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$/i) || email.lenght < 4){
                    $('form .email p span').html(unvalidEmail);
                    $('form .email p').addClass('unvalid');
                    $('form .email p').css('display','block');
                } else {
                    $('form .email input').addClass('valid-input');
                    validEmail = true;
                }

                if (!message){
                    $('form .message p span').html(emptyMessage);
                    $('form .message p').addClass('unvalid');
                    $('form .message p').css('display','block');
                } else {
                    $('form .message textarea').addClass('valid-input');
                    validMessage = true;
                }

            }

            if ( validName && validPhone && validEmail && validMessage ){
                $('.form form').css({
                    'animation': 'formSend 2s',
                    '-webkit-animation': 'formSend 2s',
                    '-webkit-animation-fill-mode': 'forwards'
                });
                $('.form h3').css({
                    'opacity': '0',
                    'transition': '1s'
                });
                $('.form h3.thanks').css({
                    'animation': 'textSend 2s',
                    '-webkit-animation': 'textSend 2s',
                    'animation-delay': '.4s',
                    '-webkit-animation-delay': '.4s',
                    '-webkit-animation-fill-mode': 'forwards'
                });
                $('.form span.send').css({
                    'animation': 'textSend 2s',
                    '-webkit-animation': 'textSend 2s',
                    'animation-delay': '.4s',
                    '-webkit-animation-delay': '.4s',
                    '-webkit-animation-fill-mode': 'forwards'
                });
            }

    });

});

